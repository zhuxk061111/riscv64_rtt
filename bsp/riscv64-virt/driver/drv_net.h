/*
 * Copyright (c) 2019-2020, Xim
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */
#ifndef NET_H
#define NET_H
int rt_net_hw_init(void);

#endif //NET_H
