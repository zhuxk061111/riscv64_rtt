/*
 * Copyright (c) 2019-2020, Xim
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */
#ifndef __DRV_SD_H__
#define __DRV_SD_H__

int rt_hw_sd_init(void);

#endif /* __DRV_SD_H__ */